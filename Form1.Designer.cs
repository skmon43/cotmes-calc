﻿namespace WindowsFormsApp3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.เรมใหมToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ออกจากโปรแกรมToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.วธใชToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.วธการคดคะแนนToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.หนาหลกขของกสพทToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.กสพทบนทวตเตอรToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา2560ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา2560ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25592ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25591ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25582ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25581ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25572ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25571ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25562ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25561ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25552ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ปการศกษา25551ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.กสพทบนทวตเตอรToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.เกยวกบToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.สงขอเสนอแนะToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(227, 216);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "คะแนน 9 วิชาสามัญ";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(201, 186);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(12, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "x";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(201, 159);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "x";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(201, 133);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "x";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(201, 107);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(12, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "x";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(201, 81);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(12, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "x";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(201, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(12, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "x";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(201, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "x";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(97, 183);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(97, 20);
            this.textBox7.TabIndex = 1;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(97, 157);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(97, 20);
            this.textBox6.TabIndex = 1;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(97, 131);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(97, 20);
            this.textBox5.TabIndex = 1;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(97, 105);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(97, 20);
            this.textBox4.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(97, 79);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(97, 20);
            this.textBox3.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(97, 53);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(97, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(97, 27);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(97, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "สังคมฯ";
            this.label7.Click += new System.EventHandler(this.label2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "ภาษาไทย";
            this.label6.Click += new System.EventHandler(this.label2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "ภาษาอังกฤษ";
            this.label5.Click += new System.EventHandler(this.label2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "ชีววิทยา";
            this.label4.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "เคมี";
            this.label3.Click += new System.EventHandler(this.label2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "ฟิสิกส์";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "คณิตศาสตร์ 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.textBox14);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Location = new System.Drawing.Point(252, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(240, 125);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "คะแนน ความถนัดแพทย์";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(214, 82);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "x";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(214, 55);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(12, 13);
            this.label23.TabIndex = 5;
            this.label23.Text = "x";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(214, 29);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(12, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "x";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(109, 79);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(97, 20);
            this.textBox12.TabIndex = 1;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(109, 53);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(97, 20);
            this.textBox13.TabIndex = 1;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(109, 27);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(97, 20);
            this.textBox14.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "ความคิดเชื่อมโยง";
            this.label12.Click += new System.EventHandler(this.label2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "จริยธรรมทางแพทย์";
            this.label13.Click += new System.EventHandler(this.label2_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "เชาว์ปัญญา";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.textBox10);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(12, 264);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(227, 67);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "คะแนน O-NET";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(202, 29);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(12, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "x";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(97, 27);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(97, 20);
            this.textBox10.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "คะแนนรวม";
            this.label8.Click += new System.EventHandler(this.label2_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.วธใชToolStripMenuItem,
            this.เกยวกบToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(504, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.เรมใหมToolStripMenuItem,
            this.ออกจากโปรแกรมToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.fileToolStripMenuItem.Text = "โปรแกรม";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // เรมใหมToolStripMenuItem
            // 
            this.เรมใหมToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("เรมใหมToolStripMenuItem.Image")));
            this.เรมใหมToolStripMenuItem.Name = "เรมใหมToolStripMenuItem";
            this.เรมใหมToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.เรมใหมToolStripMenuItem.Text = "เริ่มใหม่";
            this.เรมใหมToolStripMenuItem.Click += new System.EventHandler(this.เรมใหมToolStripMenuItem_Click);
            // 
            // ออกจากโปรแกรมToolStripMenuItem
            // 
            this.ออกจากโปรแกรมToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ออกจากโปรแกรมToolStripMenuItem.Image")));
            this.ออกจากโปรแกรมToolStripMenuItem.Name = "ออกจากโปรแกรมToolStripMenuItem";
            this.ออกจากโปรแกรมToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.ออกจากโปรแกรมToolStripMenuItem.Text = "ออกจากโปรแกรม";
            this.ออกจากโปรแกรมToolStripMenuItem.Click += new System.EventHandler(this.ออกจากโปรแกรมToolStripMenuItem_Click);
            // 
            // วธใชToolStripMenuItem
            // 
            this.วธใชToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.วธการคดคะแนนToolStripMenuItem,
            this.หนาหลกขของกสพทToolStripMenuItem,
            this.กสพทบนทวตเตอรToolStripMenuItem,
            this.กสพทบนทวตเตอรToolStripMenuItem1});
            this.วธใชToolStripMenuItem.Name = "วธใชToolStripMenuItem";
            this.วธใชToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.วธใชToolStripMenuItem.Text = "ช่วยเหลือ";
            // 
            // วธการคดคะแนนToolStripMenuItem
            // 
            this.วธการคดคะแนนToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("วธการคดคะแนนToolStripMenuItem.Image")));
            this.วธการคดคะแนนToolStripMenuItem.Name = "วธการคดคะแนนToolStripMenuItem";
            this.วธการคดคะแนนToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.วธการคดคะแนนToolStripMenuItem.Text = "วิธีการคิดคะแนน";
            this.วธการคดคะแนนToolStripMenuItem.Click += new System.EventHandler(this.วธการคดคะแนนToolStripMenuItem_Click);
            // 
            // หนาหลกขของกสพทToolStripMenuItem
            // 
            this.หนาหลกขของกสพทToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("หนาหลกขของกสพทToolStripMenuItem.Image")));
            this.หนาหลกขของกสพทToolStripMenuItem.Name = "หนาหลกขของกสพทToolStripMenuItem";
            this.หนาหลกขของกสพทToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.หนาหลกขของกสพทToolStripMenuItem.Text = "หน้าหลักของ กสพท.";
            this.หนาหลกขของกสพทToolStripMenuItem.Click += new System.EventHandler(this.หนาหลกขของกสพทToolStripMenuItem_Click);
            // 
            // กสพทบนทวตเตอรToolStripMenuItem
            // 
            this.กสพทบนทวตเตอรToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.ปการศกษา2560ToolStripMenuItem,
            this.ปการศกษา2560ToolStripMenuItem1,
            this.ปการศกษา25592ToolStripMenuItem,
            this.ปการศกษา25591ToolStripMenuItem,
            this.ปการศกษา25582ToolStripMenuItem,
            this.ปการศกษา25581ToolStripMenuItem,
            this.ปการศกษา25572ToolStripMenuItem,
            this.ปการศกษา25571ToolStripMenuItem,
            this.ปการศกษา25562ToolStripMenuItem,
            this.ปการศกษา25561ToolStripMenuItem,
            this.ปการศกษา25552ToolStripMenuItem,
            this.ปการศกษา25551ToolStripMenuItem});
            this.กสพทบนทวตเตอรToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("กสพทบนทวตเตอรToolStripMenuItem.Image")));
            this.กสพทบนทวตเตอรToolStripMenuItem.Name = "กสพทบนทวตเตอรToolStripMenuItem";
            this.กสพทบนทวตเตอรToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.กสพทบนทวตเตอรToolStripMenuItem.Text = "สถิติคะแนนสอบ";
            this.กสพทบนทวตเตอรToolStripMenuItem.Click += new System.EventHandler(this.กสพทบนทวตเตอรToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem1.Text = "ปีการศึกษา 2561 รอบ 3/2";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem2.Text = "ปีการศึกษา 2561 รอบ 3/1";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // ปการศกษา2560ToolStripMenuItem
            // 
            this.ปการศกษา2560ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา2560ToolStripMenuItem.Image")));
            this.ปการศกษา2560ToolStripMenuItem.Name = "ปการศกษา2560ToolStripMenuItem";
            this.ปการศกษา2560ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา2560ToolStripMenuItem.Text = "ปีการศึกษา 2560/2";
            this.ปการศกษา2560ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา2560ToolStripMenuItem_Click);
            // 
            // ปการศกษา2560ToolStripMenuItem1
            // 
            this.ปการศกษา2560ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา2560ToolStripMenuItem1.Image")));
            this.ปการศกษา2560ToolStripMenuItem1.Name = "ปการศกษา2560ToolStripMenuItem1";
            this.ปการศกษา2560ToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา2560ToolStripMenuItem1.Text = "ปีการศึกษา 2560/1";
            this.ปการศกษา2560ToolStripMenuItem1.Click += new System.EventHandler(this.ปการศกษา2560ToolStripMenuItem1_Click);
            // 
            // ปการศกษา25592ToolStripMenuItem
            // 
            this.ปการศกษา25592ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25592ToolStripMenuItem.Image")));
            this.ปการศกษา25592ToolStripMenuItem.Name = "ปการศกษา25592ToolStripMenuItem";
            this.ปการศกษา25592ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25592ToolStripMenuItem.Text = "ปีการศึกษา 2559/2";
            this.ปการศกษา25592ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25592ToolStripMenuItem_Click);
            // 
            // ปการศกษา25591ToolStripMenuItem
            // 
            this.ปการศกษา25591ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25591ToolStripMenuItem.Image")));
            this.ปการศกษา25591ToolStripMenuItem.Name = "ปการศกษา25591ToolStripMenuItem";
            this.ปการศกษา25591ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25591ToolStripMenuItem.Text = "ปีการศึกษา 2559/1";
            this.ปการศกษา25591ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25591ToolStripMenuItem_Click);
            // 
            // ปการศกษา25582ToolStripMenuItem
            // 
            this.ปการศกษา25582ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25582ToolStripMenuItem.Image")));
            this.ปการศกษา25582ToolStripMenuItem.Name = "ปการศกษา25582ToolStripMenuItem";
            this.ปการศกษา25582ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25582ToolStripMenuItem.Text = "ปีการศึกษา 2558/2";
            this.ปการศกษา25582ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25582ToolStripMenuItem_Click);
            // 
            // ปการศกษา25581ToolStripMenuItem
            // 
            this.ปการศกษา25581ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25581ToolStripMenuItem.Image")));
            this.ปการศกษา25581ToolStripMenuItem.Name = "ปการศกษา25581ToolStripMenuItem";
            this.ปการศกษา25581ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25581ToolStripMenuItem.Text = "ปีการศึกษา 2558/1";
            this.ปการศกษา25581ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25581ToolStripMenuItem_Click);
            // 
            // ปการศกษา25572ToolStripMenuItem
            // 
            this.ปการศกษา25572ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25572ToolStripMenuItem.Image")));
            this.ปการศกษา25572ToolStripMenuItem.Name = "ปการศกษา25572ToolStripMenuItem";
            this.ปการศกษา25572ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25572ToolStripMenuItem.Text = "ปีการศึกษา 2557/2";
            this.ปการศกษา25572ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25572ToolStripMenuItem_Click);
            // 
            // ปการศกษา25571ToolStripMenuItem
            // 
            this.ปการศกษา25571ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25571ToolStripMenuItem.Image")));
            this.ปการศกษา25571ToolStripMenuItem.Name = "ปการศกษา25571ToolStripMenuItem";
            this.ปการศกษา25571ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25571ToolStripMenuItem.Text = "ปีการศึกษา 2557/1";
            this.ปการศกษา25571ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25571ToolStripMenuItem_Click);
            // 
            // ปการศกษา25562ToolStripMenuItem
            // 
            this.ปการศกษา25562ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25562ToolStripMenuItem.Image")));
            this.ปการศกษา25562ToolStripMenuItem.Name = "ปการศกษา25562ToolStripMenuItem";
            this.ปการศกษา25562ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25562ToolStripMenuItem.Text = "ปีการศึกษา 2556/2";
            this.ปการศกษา25562ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25562ToolStripMenuItem_Click);
            // 
            // ปการศกษา25561ToolStripMenuItem
            // 
            this.ปการศกษา25561ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25561ToolStripMenuItem.Image")));
            this.ปการศกษา25561ToolStripMenuItem.Name = "ปการศกษา25561ToolStripMenuItem";
            this.ปการศกษา25561ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25561ToolStripMenuItem.Text = "ปีการศึกษา 2556/1";
            this.ปการศกษา25561ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25561ToolStripMenuItem_Click);
            // 
            // ปการศกษา25552ToolStripMenuItem
            // 
            this.ปการศกษา25552ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25552ToolStripMenuItem.Image")));
            this.ปการศกษา25552ToolStripMenuItem.Name = "ปการศกษา25552ToolStripMenuItem";
            this.ปการศกษา25552ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25552ToolStripMenuItem.Text = "ปีการศึกษา 2555/2";
            this.ปการศกษา25552ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25552ToolStripMenuItem_Click);
            // 
            // ปการศกษา25551ToolStripMenuItem
            // 
            this.ปการศกษา25551ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ปการศกษา25551ToolStripMenuItem.Image")));
            this.ปการศกษา25551ToolStripMenuItem.Name = "ปการศกษา25551ToolStripMenuItem";
            this.ปการศกษา25551ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ปการศกษา25551ToolStripMenuItem.Text = "ปีการศึกษา 2555/1";
            this.ปการศกษา25551ToolStripMenuItem.Click += new System.EventHandler(this.ปการศกษา25551ToolStripMenuItem_Click);
            // 
            // กสพทบนทวตเตอรToolStripMenuItem1
            // 
            this.กสพทบนทวตเตอรToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("กสพทบนทวตเตอรToolStripMenuItem1.Image")));
            this.กสพทบนทวตเตอรToolStripMenuItem1.Name = "กสพทบนทวตเตอรToolStripMenuItem1";
            this.กสพทบนทวตเตอรToolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
            this.กสพทบนทวตเตอรToolStripMenuItem1.Text = "#กสพท บนทวิตเตอร์";
            this.กสพทบนทวตเตอรToolStripMenuItem1.Click += new System.EventHandler(this.กสพทบนทวตเตอรToolStripMenuItem1_Click);
            // 
            // เกยวกบToolStripMenuItem
            // 
            this.เกยวกบToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.สงขอเสนอแนะToolStripMenuItem,
            this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem});
            this.เกยวกบToolStripMenuItem.Name = "เกยวกบToolStripMenuItem";
            this.เกยวกบToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.เกยวกบToolStripMenuItem.Text = "เกี่ยวกับ";
            // 
            // สงขอเสนอแนะToolStripMenuItem
            // 
            this.สงขอเสนอแนะToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("สงขอเสนอแนะToolStripMenuItem.Image")));
            this.สงขอเสนอแนะToolStripMenuItem.Name = "สงขอเสนอแนะToolStripMenuItem";
            this.สงขอเสนอแนะToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.สงขอเสนอแนะToolStripMenuItem.Text = "ส่งข้อเสนอแนะ";
            this.สงขอเสนอแนะToolStripMenuItem.Click += new System.EventHandler(this.สงขอเสนอแนะToolStripMenuItem_Click);
            // 
            // เกยวกบคำนวณคะแนนกสพทToolStripMenuItem
            // 
            this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("เกยวกบคำนวณคะแนนกสพทToolStripMenuItem.Image")));
            this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem.Name = "เกยวกบคำนวณคะแนนกสพทToolStripMenuItem";
            this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem.Text = "เกี่ยวกับ \'คำนวณคะแนน กสพท.\'";
            this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem.Click += new System.EventHandler(this.เกยวกบคำนวณคะแนนกสพทToolStripMenuItem_Click);
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox8.Location = new System.Drawing.Point(319, 282);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 31);
            this.textBox8.TabIndex = 3;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(338, 262);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "คะแนนรวม";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(347, 317);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "คะแนน";
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(332, 181);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 39);
            this.button1.TabIndex = 4;
            this.button1.Text = "คำนวณ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(504, 354);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "คำนวณคะแนน กสพท.";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem เรมใหมToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ออกจากโปรแกรมToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem วธใชToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem วธการคดคะแนนToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem หนาหลกขของกสพทToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem กสพทบนทวตเตอรToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem กสพทบนทวตเตอรToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem เกยวกบToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem สงขอเสนอแนะToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem เกยวกบคำนวณคะแนนกสพทToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา2560ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา2560ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25592ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25591ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25582ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25581ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25572ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25571ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25562ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25561ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25552ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ปการศกษา25551ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
    }
}

