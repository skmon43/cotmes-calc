﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void เรมใหมToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox10.Clear();
            textBox12.Clear();
            textBox13.Clear();
            textBox14.Clear();
            
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox8.ReadOnly = true;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            double math, phys, chem, bio, eng, thai, soci;
            double aptA, aptB, aptC;
            double ninesubj, allapt, onet, result;

            ////////////////////////////////////////////////

            if (!double.TryParse(textBox1.Text, out math))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox2.Text, out phys))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox3.Text, out chem))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox4.Text, out bio))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox5.Text, out eng))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox6.Text, out thai))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox7.Text, out soci))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox14.Text, out aptA))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox13.Text, out aptB))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox12.Text, out aptC))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            if (!double.TryParse(textBox10.Text, out onet))
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง");
                textBox8.Clear();
                return;
            }

            /////////////////////////////////////////////////////////

            if (onet > 500)
            {
                MessageBox.Show("คะแนนรวม O-NET ต้องอยู่ระหว่าง 0-500 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (onet < 0)
            {
                MessageBox.Show("คะแนนรวม O-NET ต้องอยู่ระหว่าง 0-500 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (math > 100)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (math < 0)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (phys > 100)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (phys < 0)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (chem > 100)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (chem < 0)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (bio > 100)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (bio < 0)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (eng > 100)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (eng < 0)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (thai > 100)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (thai < 0)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (soci > 100)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (soci < 0)
            {
                MessageBox.Show("คะแนน 9 วิชาสามัญต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (aptA > 100)
            {
                MessageBox.Show("คะแนนความถนัดแพทย์ต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (aptA < 0)
            {
                MessageBox.Show("คะแนนความถนัดแพทย์ต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (aptB > 100)
            {
                MessageBox.Show("คะแนนความถนัดแพทย์ต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (aptB < 0)
            {
                MessageBox.Show("คะแนนความถนัดแพทย์ต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (aptC > 100)
            {
                MessageBox.Show("คะแนนความถนัดแพทย์ต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else if (aptC < 0)
            {
                MessageBox.Show("คะแนนความถนัดแพทย์ต้องอยู่ระหว่าง 0-100 คะแนน");
                textBox8.Clear();
                return;
            }

            else
            {
                ninesubj = (math * 0.2 + ((phys + chem + bio) * 0.4 / 3) + eng * 0.2 + thai * 0.1 + soci * 0.1) * 0.7;
                allapt = ((aptA + aptB + aptC) / 3) * 0.3;
                result = ninesubj + allapt;
                textBox8.Text = result.ToString("#,###.0000");
            }

            ////////////////////////////////////////////////////////

            if (onet < 300)
            {
                MessageBox.Show("คะแนน O-NET ของคุณไม่ถึงเกณฑ์ขั้นต่ำ 60% (300 คะแนน)");
            }

            if (math < 30)
            {
                MessageBox.Show("คะแนนคณิตศาสตร์ของคุณไม่ถึงเกณฑ์ขั้นต่ำ 30% (300 คะแนน)");
            }

            if (phys < 30)
            {
                MessageBox.Show("คะแนนฟิสิกส์ของคุณไม่ถึงเกณฑ์ขั้นต่ำ 30%");
            }

            if (chem < 30)
            {
                MessageBox.Show("คะแนนเคมีของคุณไม่ถึงเกณฑ์ขั้นต่ำ 30%");
            }

            if (bio < 30)
            {
                MessageBox.Show("คะแนนชีววิทยาของคุณไม่ถึงเกณฑ์ขั้นต่ำ 30%");
            }

            if (eng < 30)
            {
                MessageBox.Show("คะแนนภาษาอังกฤษของคุณไม่ถึงเกณฑ์ขั้นต่ำ 30%");
            }

            if (thai < 30)
            {
                MessageBox.Show("คะแนนภาษาไทยของคุณไม่ถึงเกณฑ์ขั้นต่ำ 30%");
            }

            if (soci < 30)
            {
                MessageBox.Show("คะแนนสังคมฯ ของคุณไม่ถึงเกณฑ์ขั้นต่ำ 30%");
            }

            if (result == 100)
            {
                MessageBox.Show("เก่งสัส!");
            }

            ///////////////////////////////////////////////////////



        }

        private void ออกจากโปรแกรมToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void หนาหลกขของกสพทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www9.si.mahidol.ac.th/");
        }

        private void กสพทบนทวตเตอรToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://twitter.com/hashtag/%E0%B8%81%E0%B8%AA%E0%B8%9E%E0%B8%97");
        }

        private void กสพทบนทวตเตอรToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void ปการศกษา2560ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\cotmes60_p11_25600331.pdf");
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\cotmes61_14_25610605.pdf");

        }
        private void ปการศกษา2560ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\cotmes60_p8_25600314u.pdf");
        }

        private void ปการศกษา25592ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\cotmes59_10_25590324.pdf");
        }

        private void ปการศกษา25591ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\cotmes59_07_25590310.pdf");
        }

        private void ปการศกษา25582ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\cotmes_announce_12_25580310.pdf");
        }

        private void ปการศกษา25581ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\consort2558_ann09_25580223.pdf");
        }

        private void ปการศกษา25572ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\consort2557_ann15_25570401.pdf");
        }

        private void ปการศกษา25571ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\consort2557_ann12_25570314.pdf");
        }

        private void ปการศกษา25562ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\consort2556_ann12_25560227.pdf");
        }

        private void ปการศกษา25561ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\consort2556_ann08_25560211.pdf");
        }

        private void ปการศกษา25552ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\consort2555_ann17_25550227.pdf");
        }

        private void ปการศกษา25551ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\consort2555_ann14_25550217.pdf");
        }

        private void วธการคดคะแนนToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            textBox14.Clear();
        }

        private void label11_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
        }

        private void label16_Click(object sender, EventArgs e)
        {
            textBox3.Clear();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            textBox4.Clear();
        }

        private void label18_Click(object sender, EventArgs e)
        {
            textBox5.Clear();
        }

        private void label19_Click(object sender, EventArgs e)
        {
            textBox6.Clear();
        }

        private void label20_Click(object sender, EventArgs e)
        {
            textBox7.Clear();
        }

        private void label23_Click(object sender, EventArgs e)
        {
            textBox13.Clear();
        }

        private void label24_Click(object sender, EventArgs e)
        {
            textBox12.Clear();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            textBox10.Clear();
        }

        private void เกยวกบคำนวณคะแนนกสพทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.Show();           
        }

        private void สงขอเสนอแนะToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:skmon43@yahoo.com");
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"cotmes_stat\cotmes61_09_25610526.pdf");
        }
    }
}
